module main

import os
import regex

const colors = [
		'red',
		'green',
		'yellow',
		'blue',
		'magenta',
		'cyan',
		'white',
		'bright_red',
		'bright_green',
		'bright_yellow',
		'bright_blue',
		'bright_magenta',
		'bright_cyan',
		'bright_white',
	]

const wm = [
		'fluxbox',
		'openbox',
		'blackbox',
		'xfwm4',
		'metacity',
		'kwin',
		'twin',
		'icewm',
		'pekwm',
		'flwm',
		'flwm_topside',
		'fvwm',
		'dwm',
		'awesome',
		'wmaker',
		'stumpwm',
		'musca',
		'xmonad',
		'i3',
		'ratpoison',
		'scrotwm',
		'spectrwm',
		'wmfs',
		'wmii',
		'beryl',
		'subtle',
		'e16',
		'enlightenment',
		'sawfish',
		'emerald',
		'monsterwm',
		'dminiwm',
		'compiz',
		'Finder',
		'herbstluftwm',
		'howm',
		'notion',
		'bspwm',
		'2bwm',
		'echinus',
		'swm',
		'budgie-wm',
		'dtwm',
		'9wm',
		'chromeos-wm',
		'deepin-wm',
		'sway',
		'mwm',
		'instawm',
	]

fn get_os() string {
	res := os.exec('lsb_release -sd') or { panic(err) }
	return res.output
}

fn de() string {
	de := os.getenv('XDG_CURRENT_DESKTOP')
	return de
}

fn shell() string {
	shell := os.getenv('SHELL')
	return shell
}

fn kernel() string {
	return os.uname().release
}

fn packages() int {
	packages := os.exec('pacman -Q | wc -l 2>/dev/null') or { panic(err) }
	return packages.output.int()
}

fn uptime() string {
	uptime := os.exec('uptime -p') or { panic(err) }
	query := r'up (\d+) days, (\d+) hours, (\d+) minutes'
	mut re := regex.regex_opt(query) or { panic(err) }

	txt := uptime.output
	start, _ := re.match_string(txt)
	if start < 0 {
		panic('Error while parsing uptime')
	}

	mut times := []int{}
	for g_index := 0; g_index < re.group_count; g_index++ {
		start_index, end_index := re.get_group_bounds_by_id(g_index)
		times << txt[start_index..end_index].int()
	}
	return '${times[0]}d, ${times[1]}h, ${times[2]}m'
}

fn usage() {}

fn main() {
	u_os := get_os()
	krnl := kernel()
	wm := de()
	sh := shell()
	up := uptime()
	pkgs := packages()
	println(uptime())
}
